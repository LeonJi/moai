# Moai: Material Management 素材管理系統

![](http://i.imgur.com/dNx8eEI.png)

## Services

API Server
- 上傳影片、圖片
- 組成素材包

待研究
- 將影片檔案轉檔為不同大小(ffmpeg)
- 轉檔設定為排程(sidekiq)

## Models

#### `Material::Pack`
- 素材包
- 多對多video、banner
- 一對多link、tracker

```ruby
t.string  :title
t.string  :description
```

```ruby
has_many :videos
has_many :video_packs
has_many :banners
has_many :banner_packs
has_many :links
has_many :trackers
```

#### `Material::Video`
- 影片原檔
- 一對多clip

```ruby
t.string  :title
t.text    :description
t.integer :length
t.string  :file
t.integer :size
```

```ruby
has_many :video_packs
has_many :packs
```

#### `Material::VideoPack`
- pack與video之間的中介表

```ruby
t.integer :video_id, index: true
t.integer :pack_id,  index: true
```

```ruby
belongs_to :video
belongs_to :pack
```

#### `Material::Video::Clip`
- 不同影片大小轉檔的存放處，欄位包含`available`作為轉檔完成的確認

```ruby
t.boolean :available
t.string  :size('300k', '500k', '800k')
t.string  :file
t.integer :video_id, index: true
```

```ruby
belongs_to :video
```

##### `Material::Banner`
- 圖片

```ruby
t.string  :title
t.text    :description
t.integer :width
t.integer :height
t.integer :size
t.string  :file
```

```ruby
has_many :banner_packs
has_many :packs
```

#### `Material::BannerPack`
- pack與banner之間的中介表

```ruby
t.integer :banner_id, index: true
t.integer :pack_id,   index: true
```

```ruby
belongs_to :banner
belongs_to :pack
```

#### `Material::URL`
- video及banner的連結

```ruby
t.string  :material_type
t.integer :material_id, index: true
t.string  :device_type
t.text    :click_url
```

```ruby
belongs_to :banner
belongs_to :video
belongs_to :pack
```

#### `Material::Tracker`
- 第三方追蹤碼
- 欄位：`material_type`、`device`

```ruby
t.string  :material_type
t.string  :device_type
t.integer :pack_id, index: true
t.text    :tracking_url
```

```ruby
belongs_to :pack
```

## API Documentation

#### `GET /materials/packs`

- Input

```ruby
{
  id: Integer
}
```
- Process
  - 找相對應的 `id`
- Output
  - 回傳與`id`相對應的素材包列表
  - 素材包包含video及banner的預覽圖、連結
  - 每個素材包包含一個`Boolean`值判斷素材本身是否可再更改(若素材包已經有和正在跑的廣告活動連結，就不能改)

#### `GET /materials/packs/:id`

- Input
  - 同上，外加網址列的`:id`
- Process
  - 找相對應的 `id`
- Output

```ruby
{
  id: Integer,
  ...
}
```

#### `POST /materials/packs`

- Input

```ruby
{
  title: String,
  description: String,
  video_id: Array(Integer),
  banner_id: Array(Integer),
  click_urls: [
    {
      type: String('video'或'banner'),
      id: Integer,
      url: String
    }
  ]
  tracking_urls: [
    {
      material_type: String,
      device: String,
      codes: Array(String)
    }
  ]
}
```
- Process
  - 建立素材包
- Output
  - 同`GET /materials/packs/:id`

#### `PUT /materials/packs/:id`

- Input
  - 網址列上的`:id`以及新增時所有變數
- Process
  - 確認編號`id`的素材包是否有和任何正在跑的CPRP廣告活動連結
- Output
  - `Boolean`

#### `GET /materials/videos`

- Input

```ruby
{
  id: Integer
}
```

- Process
  - 找相對應的 `id`
- Output
  - 找相對應的 `id` video列表

```ruby
[
  {
    id: Integer,
    title: String,
    description: Text,
    length: Integer,
    size: Integer,
    preview: 預覽圖檔案位置(String),
    filepath: 檔案位置(String),
    clips: {
      '800k' => String,
      '500k' => String,
      '300k' => String
    }
  },
  ...
]
```

#### `GET /materials/videos/:id`

- Input
  - 同上，外加網址列的`id`
- Process
  - 找相對應的 `id`
- Output
  - 同上，但僅回傳一個video

#### `POST /materials/videos`

- Input

```ruby
{
  file: File,
  title: String,
  description: Text,
  id: Integer
}
```

- Process
  - 新增video，新增以後會自動轉檔
  - 驗證副檔名為mp4或flv、並檔案不能超過500MB
- Output
  - 同`GET /materials/videos/:id`

#### `PUT /materials/videos/:id`

- Input
  - 影片`id`，其他同上
- Process
  - 修改video檔名、說明
- Output
  - 同`GET /materials/videos/:id`

#### `GET /materials/banners`

- Input

```ruby
{
  id: Integer
}
```

- Process
  - 找相對應的 `id`
- Output
  - 根據`id`回傳banner列表

```ruby
[
  {
    id: Integer,
    width: String,
    height: String,
    size: Integer,
    title: String,
    description: Text,
    file: 檔案位置(String)
  },
  ...
]
```

#### `GET /materials/banners/:id`

- Input
  - 同上，外加網址列的`id`
- Process
  - 找相對應的 `id`
- Output
  - 同上，但僅回傳一個banner

#### `POST /materials/banners`

- Input

```ruby
{
  file: File,
  title: String,
  description: Text,
  id: Integer
}
```
- Process
  - 限制格式jpg、png、gif、swf、zip
  - 限制大小不能超過1MB
- Output
  - 同`GET /materials/banners/:id`

#### `PUT /materials/banners/:id`

- Input
  - 同上
- Process
  - 修改banner名稱、說明
- Output
  - 同`GET /materials/banners/:id`
