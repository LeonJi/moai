json.pack_object do
  json.id          @pack.id
  json.title       @pack.title
  json.description @pack.description
end

json.click_urls @urls do |u|
  json.url_id        u.id
  json.material_type u.material_type
  json.material_id   u.material_id
  json.device_type   u.device_type
  json.click_url     u.click_url
end

json.tracking_urls @trackings do |t|
  json.tracking_id   t.id
  json.material_type t.material_type
  json.pack_id       t.pack_id
  json.device_type   t.device_type
  json.tracking_url  t.tracking_url
end

json.banner @banner
json.video  @video

