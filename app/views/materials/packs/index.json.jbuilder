json.array! @packs do |p|
  json.id          p.id
  json.title       p.title
  json.description p.description
end