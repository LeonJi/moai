json.array! @banners do |b|
  json.id          b.id
  json.title       b.title
  json.description b.description
  json.width       b.width
  json.height      b.height
  json.size        b.size
  json.file        b.file
end