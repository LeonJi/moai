json.id          @banner.id
json.title       @banner.title
json.description @banner.description
json.width       @banner.width
json.height      @banner.height
json.size        @banner.size
json.file        @banner.file