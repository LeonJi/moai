json.id          @video.id
json.title       @video.title
json.description @video.description
json.length      @video.length
json.size        @video.size
json.file        @video.file