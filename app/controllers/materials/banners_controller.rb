class Materials::BannersController < ApplicationController

  def index
    @banners = Material::Banner.where("id in (?)", params[:ids]);
  end

  def show
    @banner = Material::Banner.find(params[:id])
  end

  def create
    @banner = Material::Banner.new(banner_params)

    if @banner.save
      render template: 'materials/banners/show.json.jbuilder'
    else
      render json: @banner.errors.full_messages
    end
  end

  def update
    @banner = Material::Banner.find(params[:id])

    if @banner.update(banner_params)
      render template: 'materials/banners/show.json.jbuilder'
    else
      render json: @banner.errors.full_messages
    end
  end

  private

  def banner_params
    params.permit(:title, :description, :width, :height, :size, :file)
  end
end
