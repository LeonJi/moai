class Materials::VideosController < ApplicationController
  # before_action :allow_csr, only: [:create]

  def index
    @videos = Material::Video.where("id in (?)", params[:ids])
  end

  def show
    @video  = Material::Video.find(params[:id])
  end

  def create
    @video  = Material::Video.new(video_params)

    if @video.save
      render template: 'materials/videos/show.json.jbuilder'
    else
      render json: @video.errors.full_messages
    end
  end

  def update
    @video  = Material::Video.find(params[:id])

    if @video.update(video_params)
      render template: 'materials/videos/show.json.jbuilder'
    else
      render json: @video.errors.full_messages
    end
  end

  private

  def video_params
    params.permit(:title, :description, :length, :file, :size)
  end

  # def allow_csr
  #   response.headers['Access-Control-Allow-Origin']  = 'http://localhost:3000'
  #   response.headers['Access-Control-Allow-Headers'] = 'X-Requested-With, Content-Type, Accept, x-csrf-token'
  # end
end
