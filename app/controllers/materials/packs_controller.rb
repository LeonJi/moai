class Materials::PacksController < ApplicationController

  def index
    @packs = Material::Pack.where("id in (?)", params[:ids])
  end

  def show
    @pack    = Material::Pack.find(params[:id])
    @banner  = @pack.banners.first
    @video   = @pack.videos.first

    if @banner.present?
        @urls      = @banner.urls
        @trackings = @banner.trackers
    end

    if @video.present?
        @urls      = (@urls      + @video.urls).uniq
        @trackings = (@trackings + @video.trackers).uniq
    end
  end

  def create
    @pack = Material::Pack.new(pack_params)

    if @pack.save
      Material::BannerPack.create!(pack_id: @pack.id, banner_id: params[:banner_id]) if params[:banner_id].present?
      Material::VideoPack.create!( pack_id: @pack.id, video_id:  params[:video_id])  if params[:video_id].present?

      @urls = Material::Url.create_link(params[:click_urls]) if params[:click_urls].present?
      @trackings = Material::Tracker.create_link(params[:tracking_urls], @pack.id) if params[:tracking_urls].present?

      render template: 'materials/packs/show.json.jbuilder'
    else
      render json: false
    end
  end

  def update
    @pack = Material::Pack.find(params[:id])

    if @pack.update(pack_params)
      if params[:banner_id].present?
        @pack.banner_packs.destroy_all
        Material::BannerPack.create!(pack_id: @pack.id, banner_id: params[:banner_id])
      end

      if params[:video_id].present?
        @pack.video_packs.destroy_all
        Material::VideoPack.create!(pack_id: @pack.id, video_id: params[:video_id])
      end

      @urls = Material::Url.update_link(params[:click_urls]) if params[:click_urls].present?
      @trackings = Material::Tracker.update_link(params[:tracking_urls], @pack.id) if params[:tracking_urls].present?

      render template: 'materials/packs/show.json.jbuilder'
    else
      render json: false
    end
  end

  private

  def pack_params
    params.permit(:title, :description)
  end
end
