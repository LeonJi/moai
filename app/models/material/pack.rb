class Material::Pack < ActiveRecord::Base
  has_many :video_packs
  has_many :videos, :through => :video_packs

  has_many :banner_packs
  has_many :banners, :through => :banner_packs

  has_many :urls,    :class_name => "Url",     :foreign_key => "material_id"
  has_many :trackers, :class_name => "Tracker", :foreign_key => "material_id"
end
