class Material::Url < ActiveRecord::Base
  belongs_to :banner, :class_name => "Banner", :foreign_key => "material_id"
  belongs_to :video,  :class_name => "Video",  :foreign_key => "material_id"
  belongs_to :pack,   :class_name => "Pack",   :foreign_key => "material_id"

  def self.create_link(click_urls)
    urls = Array.new
    click_urls.each do |url|
        urls << create!(url.to_hash)
    end
    urls
  end

  def self.update_link(click_urls)
    urls = Array.new
    click_urls.each do |url|
      click = find(url[:url_id])
      url.delete(:url_id)
      click.update!(url.to_hash)
      urls << click
    end
    urls
  end

end