class Material::Banner < ActiveRecord::Base
  require "#{Rails.root}/app/uploaders/banner_uploader"
  mount_uploader :file, BannerUploader
  validates :file, file_size: { less_than: 1.megabytes }
  before_save :update_file_attributes

  has_many :banner_packs
  has_many :packs, :through => :banner_packs

  has_many :urls,     :class_name => "Url",     :foreign_key => "material_id"
  has_many :trackers, :class_name => "Tracker", :foreign_key => "material_id"

  private

  def update_file_attributes
    if file.present? && file_changed?
      self.size = file.file.size
    end
  end

  # def base_info
  #   attributes.except('created_at', 'updated_at')
  # end

  # validate :image_size_validation, :if => "file?"
  # def file_size_validation
  #   if file.size > 1.megabytes
  #     errors.add(:base, "file should be less than 1MB")
  #   end
  # end
end
