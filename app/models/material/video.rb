class Material::Video < ActiveRecord::Base
  require "#{Rails.root}/app/uploaders/video_uploader"
  mount_uploader :file, VideoUploader
  validates :file, file_size: { less_than: 500.megabytes }
  before_save :update_file_attributes

  has_many :video_packs
  has_many :packs, :through => :video_packs

  has_many :urls,     :class_name => "Url",     :foreign_key => "material_id"
  has_many :trackers, :class_name => "Tracker", :foreign_key => "material_id"

  private

  def update_file_attributes
    if file.present? && file_changed?
      self.size = file.file.size
    end
  end
end