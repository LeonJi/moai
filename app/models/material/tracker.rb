class Material::Tracker < ActiveRecord::Base
  belongs_to :pack,   :class_name => "Pack",   :foreign_key => "material_id"

  def self.create_link(tracking_urls, pack_id)
    trackings = Array.new
    tracking_urls.each do |tracking|
        trackings << create!(tracking.to_hash.merge(pack_id: pack_id))
    end
    trackings
  end

  def self.update_link(tracking_urls, pack_id)
    trackings = Array.new
    tracking_urls.each do |tracking|
      tracking_link = find(tracking[:tracking_id])
      tracking.delete(:tracking_id)
      tracking_link.update!(tracking.to_hash.merge(pack_id: pack_id))
      trackings << tracking_link
    end
    trackings
  end
end