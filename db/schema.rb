# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160122055731) do

  create_table "material_banner_packs", force: :cascade do |t|
    t.integer  "banner_id",  limit: 4
    t.integer  "pack_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "material_banner_packs", ["banner_id"], name: "index_material_banner_packs_on_banner_id", using: :btree
  add_index "material_banner_packs", ["pack_id"], name: "index_material_banner_packs_on_pack_id", using: :btree

  create_table "material_banners", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.integer  "width",       limit: 4
    t.integer  "height",      limit: 4
    t.integer  "size",        limit: 4
    t.string   "file",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "material_packs", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "material_trackers", force: :cascade do |t|
    t.string   "material_type", limit: 255
    t.integer  "pack_id",       limit: 4
    t.string   "device_type",   limit: 255
    t.text     "tracking_url",  limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "material_trackers", ["pack_id"], name: "index_material_trackers_on_pack_id", using: :btree

  create_table "material_urls", force: :cascade do |t|
    t.string   "material_type", limit: 255
    t.integer  "material_id",   limit: 4
    t.string   "device_type",   limit: 255
    t.text     "click_url",     limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "material_urls", ["material_id"], name: "index_material_urls_on_material_id", using: :btree

  create_table "material_video_clips", force: :cascade do |t|
    t.boolean  "available",  limit: 1
    t.string   "size",       limit: 255
    t.string   "file",       limit: 255
    t.integer  "video_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "material_video_clips", ["video_id"], name: "index_material_video_clips_on_video_id", using: :btree

  create_table "material_video_packs", force: :cascade do |t|
    t.integer  "video_id",   limit: 4
    t.integer  "pack_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "material_video_packs", ["pack_id"], name: "index_material_video_packs_on_pack_id", using: :btree
  add_index "material_video_packs", ["video_id"], name: "index_material_video_packs_on_video_id", using: :btree

  create_table "material_videos", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.integer  "length",      limit: 4
    t.string   "file",        limit: 255
    t.integer  "size",        limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
