class CreateMaterialVideoPacks < ActiveRecord::Migration
  def change
    create_table :material_video_packs do |t|
      t.integer :video_id, index: true
      t.integer :pack_id,  index: true

      t.timestamps null: false
    end
  end
end
