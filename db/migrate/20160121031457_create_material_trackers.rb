class CreateMaterialTrackers < ActiveRecord::Migration
  def change
    create_table :material_trackers do |t|
      t.string  :material_type
      t.integer :pack_id, index: true
      t.string  :device_type
      t.text    :tracking_url

      t.timestamps null: false
    end
  end
end
