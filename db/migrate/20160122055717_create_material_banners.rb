class CreateMaterialBanners < ActiveRecord::Migration
  def change
    create_table :material_banners do |t|
      t.string  :title
      t.text    :description
      t.integer :width
      t.integer :height
      t.integer :size
      t.string  :file

      t.timestamps null: false
    end
  end
end