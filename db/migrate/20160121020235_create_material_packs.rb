class CreateMaterialPacks < ActiveRecord::Migration
  def change
    create_table :material_packs do |t|
      t.string  :title
      t.string  :description

      t.timestamps null: false
    end
  end
end
