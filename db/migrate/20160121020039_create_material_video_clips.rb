class CreateMaterialVideoClips < ActiveRecord::Migration
  def change
    create_table :material_video_clips do |t|
      t.boolean :available
      t.string  :size
      t.string  :file
      t.integer :video_id, index: true

      t.timestamps null: false
    end
  end
end
