class CreateMaterialVideos < ActiveRecord::Migration
  def change
    create_table :material_videos do |t|
      t.string  :title
      t.text    :description
      t.integer :length
      t.string  :file
      t.integer :size

      t.timestamps null: false
    end
  end
end
