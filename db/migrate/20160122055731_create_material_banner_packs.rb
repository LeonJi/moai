class CreateMaterialBannerPacks < ActiveRecord::Migration
  def change
    create_table :material_banner_packs do |t|
      t.integer :banner_id, index: true
      t.integer :pack_id,   index: true

      t.timestamps null: false
    end
  end
end
