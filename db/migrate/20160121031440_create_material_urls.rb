class CreateMaterialUrls < ActiveRecord::Migration
  def change
    create_table :material_urls do |t|
      t.string  :material_type
      t.integer :material_id, index: true
      t.string  :device_type
      t.text    :click_url

      t.timestamps null: false
    end
  end
end
