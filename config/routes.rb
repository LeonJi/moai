Rails.application.routes.draw do
  namespace :materials, :defaults => { :format => :json } do
    resources :packs  , only: [:index, :show, :create] do # Materials::PacksController
      member do
        post :update
      end
    end

    resources :videos , only: [:index, :show, :create] do # Materials::VideosController
    member do
        post :update
      end
    end

    resources :banners, only: [:index, :show, :create] do # Materials::BannersController
    member do
        post :update
      end
    end
  end

  root 'materials/packs#index'

  # match '/videos' => 'materials/videos#create', via: :options
end
