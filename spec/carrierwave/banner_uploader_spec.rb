require 'carrierwave/test/matchers'
require 'rails_helper'

describe BannerUploader do
  include CarrierWave::Test::Matchers

  before do
    BannerUploader.enable_processing = true
    @uploader = BannerUploader.new
    @uploader.store!(File.open('spec/fixtures/files/1.jpg'))
  end

  after do
    BannerUploader.enable_processing = false
    @uploader.remove!
  end

  context 'the version' do
    it "should scale down a landscape image to be exactly 1152 by 864 pixels" do
      expect(@uploader).to have_dimensions(1152 ,864)
    end
  end
end