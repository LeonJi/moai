require 'rails_helper'

describe "Materials::Packs", type: :request do

  before do
    @pack   = create(:pack)
    @banner = create(:banner)
  end

  it '#index' do
    get '/materials/packs', format: "json"
    expect(response).to have_http_status 200
  end

  describe 'GET /materials/packs' do
    it 'ok' do
      get '/materials/packs'
      expect(response).to have_http_status 200
    end
  end

  describe 'GET /materials/packs/:id' do
    it 'ok' do
      get '/materials/packs', id: @pack.id
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /materials/packs' do
    it 'ok' do
      post '/materials/packs'
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /materials/packs/:id' do
    it 'ok' do
      post '/materials/packs', id: @pack.id
      expect(response).to have_http_status 200
    end
  end
end
