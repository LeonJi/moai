require 'rails_helper'

describe "Materials::Banners", type: :request do

  before do
    @banner  = create(:banner)
    @banner2 = create(:banner)
  end

  describe 'GET /materials/banners' do
    it '#index' do
      get '/materials/banners', ids: [@banner.id, @banner2.id]
      expect(response).to have_http_status 200
    end
  end

  describe 'GET /materials/banners/:id' do
    it '#show' do
      get '/materials/banners', id: @banner.id
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /materials/banners' do
    it '#create' do
      post '/materials/banners'
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /materials/banners/:id' do
    it '#update' do
      post '/materials/banners', id: @banner.id
      expect(response).to have_http_status 200
    end
  end

end