require 'rails_helper'

describe "Materials::Videos", type: :request do

  before do
    @video = create(:video)
    @video2 = create(:video)
  end

  describe 'GET /materials/videos' do
    it '#index' do
      get '/materials/videos', ids: [@video.id, @video2.id]
      expect(response).to have_http_status 200
    end
  end

  describe 'GET /materials/videos/:id' do
    it '#show' do
      get '/materials/videos', id: @video.id
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /materials/videos' do
    it '#create' do
      post '/materials/videos'
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /materials/videos/:id' do
    it '#update' do
      post '/materials/videos', id: @video.id
      expect(response).to have_http_status 200
    end
  end
end