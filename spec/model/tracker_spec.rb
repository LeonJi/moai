require 'rails_helper'

describe Material::Tracker, type: :model do

  describe '.create_link' do
    before do
      @pack   = create(:pack)
      @params =[{pack_id: @pack.id, material_type: "banner", device_type: "mobile", tracking_url: "click1"},
                {pack_id: @pack.id, material_type: "video" , device_type: "devise", tracking_url: "click2"}]
    end

    it 'return create link array' do
      detail = Material::Tracker.create_link(@params, @pack.id)
      expect(detail[0][:material_type]).to eq "banner"
      expect(detail[0][:device_type]).to   eq "mobile"
      expect(detail[0][:tracking_url]).to  eq "click1"
      expect(detail[1][:material_type]).to eq "video"
      expect(detail[1][:device_type]).to   eq "devise"
      expect(detail[1][:tracking_url]).to  eq "click2"
    end
  end

  describe '.update_link' do
    before do
      @pack     = create(:pack)
      @tracking = create(:tracking, pack_id: @pack.id)
      @params   =[{tracking_id: @tracking.id, material_type: "banner_up", device_type: "mobile_up", tracking_url: "click_up"}]
    end

    it 'return update link array' do
      detail = Material::Tracker.update_link(@params, @pack.id)
      expect(detail[0][:material_type]).to eq "banner_up"
      expect(detail[0][:device_type]).to   eq "mobile_up"
      expect(detail[0][:tracking_url]).to  eq "click_up"
    end
  end
end