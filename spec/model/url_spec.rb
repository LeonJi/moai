require 'rails_helper'

describe Material::Url, type: :model do

  describe '.create_link' do
    before do
      @banner = create(:banner)
      @params =[{material_id: @banner.id, material_type: "banner", device_type: "mobile", click_url: "click1"},
                {material_id: @banner.id, material_type: "video" , device_type: "devise", click_url: "click2"}]
    end

    it 'return create link array' do
      detail = Material::Url.create_link(@params)
      expect(detail[0][:material_type]).to eq "banner"
      expect(detail[0][:device_type]).to   eq "mobile"
      expect(detail[0][:click_url]).to     eq "click1"
      expect(detail[1][:material_type]).to eq "video"
      expect(detail[1][:device_type]).to   eq "devise"
      expect(detail[1][:click_url]).to     eq "click2"
    end
  end

  describe '.update_link' do
    before do
      @banner = create(:banner)
      @url    = create(:url, material_id: @banner.id)
      @params =[{url_id: @url.id, material_type: "banner_up", device_type: "mobile_up", click_url: "click_up"}]
    end

    it 'return update link array' do
      detail = Material::Url.update_link(@params)
      expect(detail[0][:material_type]).to eq "banner_up"
      expect(detail[0][:device_type]).to   eq "mobile_up"
      expect(detail[0][:click_url]).to     eq "click_up"
    end
  end
end