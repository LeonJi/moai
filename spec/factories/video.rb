FactoryGirl.define do
  factory :video, class: "Material::Video" do
    title        "video1"
    description  "video1"
    file         Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/files/NO1.mp4')))
  end
end
