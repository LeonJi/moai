FactoryGirl.define do
  factory:url, class: "Material::Url" do
    material_type   "banner"
    device_type     "mobile"
    click_url       "click1"
  end
end