FactoryGirl.define do
  factory :banner, class: "Material::Banner" do
    title       "title1"
    description "description1"
    file         Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/files/1.jpg')))
  end
end