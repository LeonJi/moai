FactoryGirl.define do
  factory :pack, class: "Material::Pack" do
    title       "title1"
    description "description1"
  end
end
