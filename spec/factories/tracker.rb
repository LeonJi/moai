FactoryGirl.define do
  factory:tracking, class: "Material::Tracker" do
    material_type   "banner"
    device_type     "mobile"
    tracking_url    "tracking1"
  end
end