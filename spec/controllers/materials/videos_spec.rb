require 'rails_helper'

describe Materials::VideosController, type: :controller do
  render_views

  before do
    @video    = create(:video)
    @video2   = create(:video)
    @file     = fixture_file_upload('files/NO1.mp4', 'video/mp4')
    @file_jpg = fixture_file_upload('files/1.jpg', 'image/jpg')
  end

  it '#index' do
    get :index, ids: [@video.id, @video2.id], format: :json
    expect(response).to have_http_status 200
    expect(response.body).to include("11241581")
  end

  it '#show' do
    get :show, id: @video.id, format: :json
    expect(response).to      have_http_status 200
    expect(response.body).to include("11241581")
  end

  describe '#create' do
    before do
      @video_params     = {title: "video2", describe: "video2", file: @file}
      @video_params_jpg = {title: "video2", describe: "video2", file: @file_jpg}
    end

    it 'create record' do
      expect{ post :create, @video_params }.to change{ Material::Video.all.size }.by(1)
      expect(Material::Video.last.title).to include "video2"
    end

    it 'create false for wrong format' do
      post :create, @video_params_jpg
      expect(response.body).to include "allowed types: mp4, flv"
    end

    it 'create success' do
      post :create,  @video_params
      expect(response).to have_http_status 200
    end
  end

  describe '#update' do
    before do
      @video_params     = {title: "video3", describe: "video3", file: @file,     id: @video.id}
      @video_params_jpg = {title: "video3", describe: "video3", file: @file_jpg, id: @video.id}
    end

    it 'update record' do
      post :update, @video_params
      expect(Material::Video.find(@video.id)[:title]).to eq "video3"
    end

    it 'update false for wrong format' do
      post :update, @video_params_jpg
      expect(response.body).to include "allowed types: mp4, flv"
    end

    it 'update success' do
      post :update, @video_params
      expect(response).to have_http_status 200
    end
  end
end
