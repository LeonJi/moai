require 'rails_helper'

describe Materials::BannersController, type: :controller do
  render_views

  before do
    BannerUploader.enable_processing = true
    @banner   = create(:banner)
    @banner2  = create(:banner)
    @file     = fixture_file_upload('files/1.jpg', 'image/jpg')
    @file_big = fixture_file_upload('files/2.jpg', 'image/jpg')
    @file_pdf = fixture_file_upload('files/3.pdf', 'application/pdf')
  end

  it '#index' do
    get :index, ids: [@banner.id, @banner2.id], format: :json
    expect(response).to have_http_status 200
    expect(response.body).to include("1152")
    expect(response.body).to include("864")
    expect(response.body).to include("251907")
  end

  it '#show' do
    get :show, id: @banner.id, format: :json
    expect(response).to have_http_status 200
    expect(response.body).to include("1152")
    expect(response.body).to include("864")
    expect(response.body).to include("251907")
  end

  describe '#create' do
    before do
      @banner_params     = {title: "banner2", description: "describe2", file: @file}
      @banner_params_big = {title: "banner2", description: "describe2", file: @file_big}
      @banner_params_pdf = {title: "banner2", description: "describe2", file: @file_pdf}
    end

    it 'create record' do
      expect{ post :create, @banner_params }.to change{ Material::Banner.all.size }.by(1)
      expect(Material::Banner.last.title).to eq "banner2"
    end

    it 'create false for big data' do
      post :create, @banner_params_big
      expect(response.body).to include "File file size must be less than 1 MB"
    end

    it 'create false for wrong format' do
      post :create, @banner_params_pdf
      expect(response.body).to include "allowed types: jpg, png, gif, swf, zip"
    end

    it 'create success' do
      post :create,  @banner_params
      expect(response).to have_http_status 200
    end
  end

  describe '#update' do
    before do
      @banner_params     = {title: "title3", description: "describe3", file: @file,     id: @banner.id}
      @banner_params_big = {title: "title3", description: "describe3", file: @file_big, id: @banner.id}
      @banner_params_pdf = {title: "title3", description: "describe3", file: @file_pdf, id: @banner.id}
    end

    it 'update record' do
      post :update, @banner_params
      expect(Material::Banner.find(@banner.id)[:title]).to eq "title3"
    end

    it 'update false for big data' do
      post :update, @banner_params_big
      expect(response.body).to include "File file size must be less than 1 MB"
    end

    it 'update false for wrong format' do
      post :update, @banner_params_pdf
      expect(response.body).to include "allowed types: jpg, png, gif, swf, zip"
    end

    it 'update success' do
      post :update, @banner_params
      expect(response).to have_http_status 200
    end
  end
end
