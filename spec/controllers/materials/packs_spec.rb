require 'rails_helper'

describe Materials::PacksController, type: :controller do
  before do
    @pack     = create(:pack)
    @pack2    = create(:pack)
    @banner   = create(:banner)
    @video    = create(:video)
    @url      = create(:url,      material_id: @banner.id)
    @tracking = create(:tracking, pack_id: @pack.id)
  end

  it '#index' do
    get :index, format: :json, ids: [@pack.id, @pack2.id]
    expect(response).to have_http_status 200
    expect(response).to render_template(:index)
  end

  it '#show' do
    get :show, format: :json, id: @pack.id
    expect(response).to have_http_status 200
    expect(response).to render_template(:show)
  end

  describe "#create" do
    before do
      @pack_params = {title: "title2", describe: "describe2", video_id: @video.id, banner_id: @banner.id,
                      click_urls:[{material_id: @banner.id, material_type: "banner", device_type: "mobile", click_url: "click1"},
                                  {material_id: @banner.id, material_type: "video" , device_type: "devise", click_url: "click2"}],
                      tracking_urls: [{material_type: "banner", device_type: "mobile", tracking_url: "tracking1"},
                                      {material_type: "video" , device_type: "devise", tracking_url: "tracking2"}]
                      }
    end

    it 'create record' do
      expect{ post :create, @pack_params }.to change{ Material::Pack.all.size }.by(1)
      expect{ post :create, @pack_params }.to change{ Material::Url.all.size }.by(2)
      expect{ post :create, @pack_params }.to change{ Material::Tracker.all.size }.by(2)
      expect(Material::Pack.last.title).to eq 'title2'
      expect(Material::Url.last.click_url).to eq 'click2'
      expect(Material::Tracker.last.tracking_url).to eq 'tracking2'
    end

    it 'create success' do
      post :create, @pack_params
      expect(response).to have_http_status 200
    end
  end

  describe "#update" do

    before do
      @pack_params = {id: @pack.id, title: "title3", describe: "describe3", video_id: @video.id, banner_id: @banner.id,
                      click_urls:[{url_id: @url.id, material_id: @banner.id, material_type: "banner", device_type: "desktop3", click_url: "click3"}],
                      tracking_urls: [{tracking_id: @tracking.id, material_type: "banner", device_type: "desktop3", tracking_url: "tracking3"}]
                      }
    end

    it 'update record' do
      post :update, @pack_params
      expect(Material::Pack.find(@pack.id)[:title]).to eq "title3"
      expect(Material::Url.find(@url.id)[:device_type]).to eq 'desktop3'
      expect(Material::Tracker.find(@tracking.id)[:device_type]).to eq 'desktop3'
    end

    it 'update success' do
      post :update, @pack_params
      expect(response).to have_http_status 200
    end
  end
end
